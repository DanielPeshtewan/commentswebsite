function getDocument(address, source, post, destName) {
	if (destName===undefined)
		destName='#content'
		//Jeśli destname jest niezdefiniowany ustaw na <div id=#content>
	if (post===undefined)
		post=''
	const comm = new XMLHttpRequest()
	//Tworzymy zmienną comm która tworzy żądanie do przeglądarki za pomocą XMLHttpRequest
	comm.onreadystatechange= function(e) {
		console.log('OPERACJA')
		if (comm.readyState === 4) {
		//XMLHttpRequest.readyState jest równe 4 (czyli jest wykonane)
			if (comm.status === 200) {
			//XMLHttpRequest.status jest równe 200 (czyli jest wykonane)
				window.history.replaceState('', '', "?site="+source);
				document.querySelector(destName).innerHTML = comm.responseText
				//Po załadowaniu strony i przejściu na inną podstronę, przeglądarka nie przeładuje całej strony tylko od razu się wyświetli dokument				
				if (source==='kontakt')
					addFormEvents()
				//Jeśli source jest równy kontakt, użyj funkcji addFormEvents() 
			}
		}
	}
	
	comm.open('POST',address)
	comm.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	comm.send(post)
	//?
}

function addEvents(){
	document.querySelectorAll(".button").forEach( v => {	
		v.onclick = (e) => {
			getDocument('./sites/'+v.dataset.link+'.html', v.dataset.link)
			//document.location='./sites/'+v.dataset.link+'.html'
			//document.location=v.dataset.link	
		}
	})		
}

function setSite() {
	//tworzymy funkcję setSite
	const site = document.location.search.substr(1).split('=')[1]
	getDocument(`./sites/${site}.html`, site)
	/*pobierz dokument (z folderu sites/), zmienna site, powoduje zapisanie naszej ostatnio wybranej podstrony
	wykorzustujemy do tego location.search.substr(1) a z kolei getDocument, wskazuje ścieżke gdzie posiadamy nasze strony
	zmienna site, zbiera nasze strony i nazwa strony jest odpowienia wzorcowi ${site}, dzięki temu posiadamy zapis naszsej ostatnio otworzonej strony*/
}

function addFormEvents() {
//tworzymy funkcję addFormEvents
	document.querySelector("#form button").onclick=function() {
	//za pomocą querySelector pobieramy z <div id="form"> wszystkie przyciski (w tym wypadku mamy 1 btn)
		let name = document.querySelector('input[name=name]').value
		let lname = document.querySelector('input[name=lastname]').value
		let email = document.getElementsByName('email')[0].value
		let content = document.getElementsByName('content')[0].value
		//tworzenie zmiennych dla formularza, które po uzupełnieniu będą zwracane jako wartość
		var sex ='';
		document.querySelectorAll('input[name=sex]').forEach((v) => {
			if (v.checked) 
				sex=v.value
		})
		//tworzenie zmiennej dla płci oraz pętli forEach, dzięki tej pętli sprawdzamy która z opcji została wybrana a potem zwrócona jako wartość
			
			/*let name = `<p>${document.querySelector('input[name=name]').value}</p>`
			//let name = `<p>${document.getElementsByName('name')[0].value}</p>`
			let lname = `<p>${document.querySelector('input[name=lastname]').value}</p>`
			//let lname = `<p>${document.getElementsByName('lastname')[0].value}</p>`
			let email = `<p>${document.getElementsByName('email')[0].value}</p>`
			let content = `<p>${document.getElementsByName('content')[0].value}</p>`
			var sex ='';
			document.querySelectorAll('input[name=sex]').forEach((v) => {
				if (v.checked) 
					sex=`<p>${v.value}</p>`
			})*/
			/*document.getElementsByName('sex').forEach((v) => {
				if (v.checked) 
					sex=`<p>${v.value}</p>`
			})*/
			//let sex = `<p>${document.getElementsByName('sex')[2].value}</p>`
			//document.querySelector('#output').innerHTML=name+lname+email+content+sex

		//powyżej zakomentowany kod, przedstawia zbieranie danych z formularza w inny sposób niż kod który wykorzystaliśmy

		post=`name=${name}&lastname=${lname}&email=${email}&content=${content}&sex=${sex}`
		getDocument('./php/main.php','kontakt',post,'#output')
		//odwoływanie się do .php, wszystkie nasze wartości (imie, nazwisko itd.) są zwrócone jako post i odsyłane do .php
	}
}

